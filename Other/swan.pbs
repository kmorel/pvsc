#!/bin/bash
#PBS -N ParaView
#PBS -j oe
#PBS -S /bin/bash

echo "---------- BEGIN PVSERVER LAUNCHER ----------"
date +"%F %T"
echo "---------------------------------------------"

module swap PrgEnv-cray PrgEnv-intel
if [ "${PV_VERSION_FULL}" -lt "5.3.0" ]
then
  module load cray-hdf5
fi
PVBASE_DIR=$(ls -d ${HOME}/Code/ParaView/Superbuild/install/${PV_VERSION_FULL}-osmesa_intel* | head -1)
export PATH=${PVBASE_DIR}/bin:${PATH}

NUM_CPN=$(aprun lscpu | awk '$1=="CPU(s):" {print $2}')
NUM_TPC=$(aprun lscpu | awk '$1=="Thread(s)" {print $4}')

# Only use SMT on KNL
if [ ${NUM_TPC} -ne 4 ]
then
  NUM_CPN=$((NUM_CPN/NUM_TPC))
  NUM_TPC=1
fi
NUM_TPP=$((NUM_CPN/PBS_NUM_PPN))
echo "Setting ${NUM_TPP} threads per process"

export GALLIUM_DRIVER=swr
export KNOB_MAX_WORKER_THREADS=${NUM_TPP}
export OSPRAY_THREADS=${NUM_TPP}
export OMP_NUM_THREADS=${NUM_TPP}
export KMP_NUM_THREADS=${NUM_TPP}

echo "Total Tasks               : ${PBS_NP}"
echo "Nodes                     : ${PBS_NUM_NODES}"
echo "Processes Per Node        : ${PBS_NUM_PPN}"
echo "Logical CPUs Per Node     : ${NUM_CPN}"
echo "Hardware Threads Per Core : ${NUM_TPC}"
echo "Render Threads Per Process: ${NUM_TPP}"
echo "Calling: aprun -n ${PBS_NP} -N ${PBS_NUM_PPN} -d ${NUM_TPP} -j ${NUM_TPC} -cc depth"

aprun -n ${PBS_NP} -N ${PBS_NUM_PPN} -d ${NUM_TPP} -j ${NUM_TPC} -cc depth \
  pvserver \
    --use-offscreen-rendering \
    -rc \
    -ch=${PV_SERVER_HOST} \
    -sp=${PV_SERVER_PORT} \
    --timeout=$((HOURS*60))
