#!/usr/bin/env bash
# ParaView Server job submission wrapper script.
# Maintainer: Dan Lipsa @ Kitware

BASE_DIR=$(dirname $(readlink -f ${BASH_SOURCE}))
. ${BASE_DIR}/../Common/pvserver_jobsub.functions


# Print usage
usage() {
  cat<<EOF
Usage: $0 NUM_NODES MINUTES PV_SERVER_PORT PV_VERSION_FULL CONF [VAR=VAL ...]

  NUM_NODES       - Number of compute nodes to use.
  MINUTES         - Job duration in minutes.
  PV_SERVER_PORT  - Port number for pvserver to use
  PV_VERSION_FULL - ParaView version string
  HEADLESS_API    - Headless API: osmesa or egl
  CONF            - Name of configuration file to load
  VAR=VAL         - Any extra variables to export
EOF
}

# Cleanup on exit
cleanup() {
  echo "Cleaning up"
  if [ -n "${JOBID}" ]
  then
    cancel_job ${JOBID}
  fi
  cleanup_tunnel
}

# Error handling and cleanup
trap cleanup EXIT INT TERM SIGKILL

if [ $# -lt 6 ]
then
  usage
  exit 1
fi

export NUM_NODES=$1
export MINUTES=$2
export PV_SERVER_PORT=$3
export PV_VERSION_FULL=$4
export HEADLESS_API=$5
CONF=$6
shift 6

echo "Exporting additional variables"
export_vars "$@"


if [ -f ${CONF} ]
then
  CONF_PATH=${CONF}
elif [ -f ${BASE_DIR}/${CONF} ]
then
  CONF_PATH=${BASE_DIR}/${CONF}
else
  echo "Error: Config file ${CONF} does not exist"
  sleep 10
  exit 2
fi
echo "Loading config file ${CONF}"
source ${CONF_PATH}
export CONF_PATH

echo "Setting up login node tunnel"
if ! setup_tunnel_pre
then
  echo "Error setting up login node tunnel"
  sleep 10
  exit 3
fi

echo "Submitting pvserver job for ${NUM_NODES} node(s), ${MINUTES} minutes"
export DRIVER THREADS_PER_CORE SUB_IF BASE_DIR SOCAT_DIR
if ! submit_job
then
  echo "Error submitting pvserver job"
  sleep 10
  exit 4
fi

echo "Waiting for pvserver job to run"
S=0
job_status ${JOBID}
JOB_STATUS=$?
while [ ${JOB_STATUS} -eq 1 ]
do
  sleep_print 5
  job_status ${JOBID}
  JOB_STATUS=$?
done
echo ""
if [ ${JOB_STATUS} -eq 2 ]
then
  echo "Error: pvserver job unable to start"
  sleep 10
  exit 5
fi
echo "Setting up launch node tunnel"
if ! setup_tunnel_post
then
  echo "Error setting up launch node tunnel"
  sleep 10
  exit 6
fi

echo ""
echo "ParaView allocation granted. Initating reverse connection between"
echo "pvserver and your ParaView client."
echo ""

echo "Running pvserver"
S=0
job_status ${JOBID}
JOB_STATUS=$?
while [ ${JOB_STATUS} -eq 0 ]
do
  sleep_print 5
  job_status ${JOBID}
  JOB_STATUS=$?
done
echo ""
